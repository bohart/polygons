Drupal.ecayGeo.map.data.loadGeoJson("/KYD2.geojson");

Drupal.ecayGeo.containsLocation = function () {
	var status = false;

	containsLocationGeometry = function (geometry) {
		var type = geometry.getType();
		if (type == 'MultiPolygon') {
			for (i = 0; i < geometry.getLength(); i++) {
				containsLocationGeometry(geometry.getAt(i));
			}
		}
		else if (type == 'Polygon') {
		    var polyPath = geometry.getAt(0).getArray();
		    var poly = new google.maps.Polygon({paths: polyPath});
		    var local_status = google.maps.geometry.poly.containsLocation(Drupal.ecayGeo.marker.position, poly);
		    if (local_status == true) status = true;
		}
		else {
			console.log(type);
		}
	};

	Drupal.ecayGeo.map.data.forEach(function(feature) {
		containsLocationGeometry(feature.getGeometry());
	})

	return status;
};

(function() {
	var current_status = Drupal.ecayGeo.containsLocation();
	Drupal.ecayGeo.map.data.setStyle({
		fillColor: current_status == false ? 'red' : 'green',
		fillOpacity: current_status == false ? '0.1' : '0.05',
		strokeWeight: 0.5
	});
})();
